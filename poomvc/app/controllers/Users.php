<?php
require_once '../app/libraries/Controller.php';
class Users extends Controller
{
    private $userModel;

    public function __construct()
    {
        $this->userModel = $this->model('User');
    }



    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            //Lo haremos más tarde

            // sanitizacion 
            $args = array(
                'name' => FILTER_SANITIZE_STRING,
                'email' => FILTER_SANITIZE_EMAIL,
                'password' => FILTER_SANITIZE_STRING,
                'confirm_password' => FILTER_SANITIZE_STRING
            );

            $data = filter_input_array(INPUT_POST, $args);


            // quitar los espacios en blanco
            $caracteres = "\t\n\r\0\x0B";
            $data['name'] = trim($data['name'], $caracteres);
            $data['email'] = trim($data['email'], $caracteres);
            $data['password'] = trim($data['password'], $caracteres);
            $data['confirm_password'] = trim($data['confirm_password'], $caracteres);

            // Elimina backslashes \

            $data['name'] = stripslashes($data['name']);
            $data['email'] = stripslashes($data['email']);
            $data['password'] = stripslashes($data['password']);
            $data['confirm_password'] = stripslashes($data['confirm_password']);

            // traduce caracteres especiales en entidades HTML

            $data['name'] = htmlspecialchars($data['name']);
            $data['email'] = htmlspecialchars($data['email']);
            $data['password'] = htmlspecialchars($data['password']);
            $data['confirm_password'] = htmlspecialchars($data['confirm_password']);





            // comprobamos que nombre, email, password y confirm_password no esten vacios
            $flag_err = true; // controla si alguno de los campos tiene errores;
            if (empty($data['name'])) {
                $data['name_err'] = "El campo nombre está vacío";

                $flag_err = false;
            } else {
                $data['name_err'] = $data['name'];
            }

            if (empty($data['email'])) {
                $data['email_err'] = "El campo Email está vacío";
                $flag_err = false;
            } else {
                $data['email_err'] = $data['email'];
            }

            if (empty($data['password'])) {
                $data['password_err'] = "El campo password está vacío";
                $flag_err = false;
            }

            if (!empty($data['password']) && !empty($data['confirm_password'])) {
                if ($data['password'] !== $data['confirm_password']) {
                    $data['confirm_password_err'] = 'Las contraseñas no son iguales';
                    $flag_err = false;
                }
            } else {
                $flag_err = false;
            }

            if (!empty($data['password']) && (strlen($data['password']) < 6)) {
                $data['password_err'] = 'El password tiene que tener  6 caracteres';
                $flag_err = false;
            }




            // comprobamos si en usuario que se quiere registrar existe en la base de usuario

            if ($flag_err) {


                // no devuelve un booleano
                $resultado = $this->userModel->findUserByEmail($data['email']);
                echo "</br>";
                if ($resultado) {

                    echo "el usuario está en la base de datos";
                } else {
                    echo "el usuario no está en la base de datos";
                }

                echo "</br>";
                echo "To-do:registrar usuario";
            } else {

                $this->view('users/register', $data);
            }
        } else {



            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''
            ];
            $this->view('users/register', $data);
        }
    }

    public function login()
    {


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            //Lo haremos más tarde

            $flag_err = false;

            // sanitizacion 

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);



            // quitar los espacios en blanco

            $caracteres = "\t\n\r\0\x0B";
            $data['email'] = trim($_POST['email'], $caracteres);
            $data['password'] = trim($_POST['password'], $caracteres);


            // Elimina backslashes \

            $data['email'] = stripslashes($data['email']);
            $data['pasword'] = stripslashes($data['password']);


            // traduce caracteres especiales en entidades HTML

            $data['email'] = htmlspecialchars($data['email']);
            $data['password'] = htmlspecialchars($data['password']);

            if (empty($data['email'])) {
                $data['email_err'] = "El campo email esta  vacío";
                $flag_err = true;
            }

            if (empty($data['password'])) {
                $data['password_err'] = "El campo password está vacío";
                $flag_err = true;
            }
            if (!empty($data['password']) && (strlen($data['password']) < 6)) {
                $data['password_err'] = 'Se necesitan 6 caracteres como mínimo';
                $flag_err = true;
            }

            if ($flag_err) {

                $this->view('users/login', $data);
            } else {
                echo " ya está logueado";
            }
        } else {
            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];
            $this->view('users/login', $data);
        }
    }
    public function logout()
    {
    }
}
