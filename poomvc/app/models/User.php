<?php

class User
{
    
    private $db;

    public function __construct()
    {
        // instanciamos  a Database
        $this->db = new Database();
    }

    // método que nos permite la busqueda en la tabla users del usuario que tiene un email en concreto

    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * from users WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->obtnerUsuario('User'); 

        if (empty($row)) {
            return false;
        } else {
           
            return true;
        }
        
        
    }

   

    
}
