<?php
//CONTROLADOR

//require_once "../config/config.php";
/*
define('DB_HOST','192.168.56.101');

define('DB_USER','camilo');

define('DB_PASS','1234');

define('DB_NAME','poomvc');

define('DB_PORT','80')

*/

// CLASE Database

class Database{

private $host = DB_HOST;

private $user = DB_USER;

private $pass = DB_PASS;

private $dbname = DB_NAME;

private $port = DB_PORT;

private $dbh; 

private $stmt;

private $error;

// constructor

public function __construct()

{

 $dsn = 'mysql:host='.$this->host.'; port='.$this->port.'; dbname='.$this->dbname;

 $options = [


 PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8",

 PDO::ATTR_PERSISTENT => true, 

 PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION

 ];




 try {

 $this->dbh = new PDO($dsn, $this->user, $this->pass, $options);
 echo "conexion exitosa";
 } catch (PDOException $e) {

 $this->error = $e->getMessage();
 echo "conexion no exitosa";
 echo $this->error;

 }

}

// funcion que se encarga de preparar las consultas.

public function query (string $SQL){

   

    $this->stmt = $this->dbh->prepare($SQL); 
   
   }
   

   // funcion que se encarga de asignar los valores a los parámetros de la consulta preparada.
   public function bind($param, $value, $type=null) {

    if (is_null($type)) {
   
    switch(true) {
   
    case is_int($value):
   
    $type = PDO::PARAM_INT;
   
    break;
   
    case is_bool($value):
   
    $type = PDO::PARAM_BOOL;
   
    break;
   
    case is_null($value):
   
    $type = PDO::PARAM_NULL;
   
    break;
   
    default:
   
    $type = PDO::PARAM_STR;
   
    }
   
    }
   
    $this->stmt->bindValue($param, $value, $type);
   
   }

// este método ejecuta la consulta de query ha preparado
   public function execute() {

    return $this->stmt->execute();
   
   }

   // método para obtener todos los resultados de la consulta
   public function resultSet($model) {

    $this->execute();
   
    return $this->stmt->fetchAll(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $model);
    
   
   }
    // método para obtener todos un resultado si la consulta tiene exito
   public function obtnerUsuario($model)
   {
     
       $this->execute();

       $this->stmt->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, $model);

      return $this->stmt->fetch();
      

    
   }


}


?>